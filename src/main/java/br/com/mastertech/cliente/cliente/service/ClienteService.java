package br.com.mastertech.cliente.cliente.service;

import br.com.mastertech.cliente.cliente.ClienteNotFoundException;
import br.com.mastertech.cliente.cliente.models.Cliente;
import br.com.mastertech.cliente.cliente.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente criar(Cliente cliente) {
        return clienteRepository.save(cliente);
    }

    public Cliente buscarClientePorId(int id) {
        Optional<Cliente> byId = clienteRepository.findById(id);

        if(!byId.isPresent()) {
            throw new ClienteNotFoundException();
        }

        return byId.get();

    }
}
