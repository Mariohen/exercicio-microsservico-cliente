package br.com.mastertech.cliente.cliente.controller;

import br.com.mastertech.cliente.cliente.models.Cliente;
import br.com.mastertech.cliente.cliente.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @PostMapping
    public Cliente criar(@RequestBody Cliente cliente) {
        return clienteService.criar(cliente);
    }

    @GetMapping("/{id}")
    public Cliente getByPlate(@PathVariable(name = "id") int id) {
        return clienteService.buscarClientePorId(id);
    }

}
